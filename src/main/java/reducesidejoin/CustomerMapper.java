package reducesidejoin;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;

public class CustomerMapper extends Mapper<LongWritable, Text, JoinKey, JoinValue> {
    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] fields = StringUtils.split(value.toString(), StringUtils.COMMA);

        JoinKey jk = new JoinKey();
        jk.setId(Integer.parseInt(fields[0]));

        JoinValue jv = new JoinValue();
        jv.setName(fields[1]);
        context.write(jk, jv);

    }
}
