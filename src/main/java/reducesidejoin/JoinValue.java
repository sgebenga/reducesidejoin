package reducesidejoin;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class JoinValue implements Writable {
    private String name;
    private double value;

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(name == null ? "" : name);
        dataOutput.writeDouble(value);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        name = dataInput.readUTF();
        value = dataInput.readDouble();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return String.format("%s,%s", name, value);
    }

    public boolean isLeft() {
        return !isRight();
    }

    public boolean isRight() {
        return "".equals(name);
    }
}
