package reducesidejoin;

import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ReduceSideJoinReducer extends Reducer<JoinKey, JoinValue, JoinKey, JoinValue> {

    public enum JoinType {
        LEFT("LEFT"), RIGHT("RIGHT"), INNER("INNER"), OUTER("OUTER");

        private String value;

        JoinType(String value) {
            this.value = value;
        }

        public String asString() {
            return this.value;
        }
    }

    ;

    private String joinType;

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        joinType = context.getConfiguration().get("reducesidejoin.join.type", JoinType.INNER.asString());
    }

    @Override
    protected void reduce(JoinKey key, Iterable<JoinValue> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        String name = "";
        boolean hasLeft = false, hasRight = false;
        for (JoinValue value : values) {
            if (value.isLeft()) {
                name = value.getName();
                hasLeft = true;
            } else if (value.isRight()) {
                acum += value.getValue();
                hasRight = true;
            }
           // hasRight = false;
        }
        if (matchesJoin(hasLeft, hasRight)) {
            JoinValue jv = new JoinValue();
            jv.setName(name);
            jv.setValue(acum);
            context.write(key, jv);
        }
    }

    private boolean matchesJoin(boolean hasLeft, boolean hasRight) {
        if (isInnerJoin() && hasLeft && hasRight) {
            return true;
        }
        if (isLeftJoin() && hasLeft && !hasRight) {
            return true;
        }
        if (isRightJoin() && !hasLeft && hasRight) {
            return true;
        }
        if (isOuterJoin() && !hasLeft && !hasRight) {
            return true;
        }
        return false;
    }

    private boolean isInnerJoin() {
        return JoinType.INNER.asString().equalsIgnoreCase(joinType);
    }

    private boolean isLeftJoin() {
        return JoinType.LEFT.asString().equalsIgnoreCase(joinType);
    }

    private boolean isRightJoin() {
        return JoinType.RIGHT.asString().equalsIgnoreCase(joinType);
    }

    private boolean isOuterJoin() {
        return JoinType.OUTER.asString().equalsIgnoreCase(joinType);
    }
}
