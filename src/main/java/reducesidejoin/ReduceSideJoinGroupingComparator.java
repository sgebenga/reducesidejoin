package reducesidejoin;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class ReduceSideJoinGroupingComparator extends WritableComparator {

    public ReduceSideJoinGroupingComparator() {
        super(JoinKey.class, true);
    }

    @Override
    public int compare(WritableComparable a, WritableComparable b) {
        JoinKey jka = (JoinKey) a;
        JoinKey jkb = (JoinKey) b;
        return Integer.compare(jka.getId(), jkb.getId());
    }
}
