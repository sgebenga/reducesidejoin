# README #

### What is this repository for? ###

* A Hadoop 2.8 reduce-side join application

### How do I get set up? ###

* $  mvn clean install ; hadoop jar target/reducesidejoin-1.0-SNAPSHOT.jar reducesidejoin.ReduceSideJoinApp data/customers data/purchases data/output
